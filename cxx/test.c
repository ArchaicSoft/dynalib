#include <stdio.h>
#include "dynalib.h"

int* c_int;
void (*print_int)(void);

#if __APPLE__
const char* libName = "./libplugin.dynalib";
#elif __linux__
const char* libName = "./libplugin.so";
#elif _WIN32
const char* libName = "./plugin.dll";
#else
const char* libName = "oof";
#endif

int main(void)
{
    HANDLE lib = loadLib(libName);
    bindSymbol(lib, (void**)&c_int, "c_int");
    bindSymbol(lib, (void**)&print_int, "c_print_int");

    printf("Printf: c_int == %d\n", *c_int);
    (*print_int)(); *c_int = 53; (*print_int)();
    getchar();
}