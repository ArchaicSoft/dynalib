module main;

version(Windows)
{
    import core.sys.windows.windows;
    import core.sys.windows.dll;
    mixin SimpleDllMain;
}

export:

    extern (D):
        __gshared int d_int = 15;

        void d_print_int() {
            import std.stdio : writeln;
            writeln("The D integer is: ", d_int);
        }
    
    extern (C):
        __gshared int c_int = 51;

        void c_print_int() {
            import core.stdc.stdio : printf;
            printf("The C integer is: %i\n", c_int);
        }