## DynaLib

This is a stand-alone single module for importing dynamic libraries.

It features both a standard 'direct-handle' loader, as you would do in C, as well as a helper struct which can retain some details about the original info used to load the library such as its name, location, and 'dependencies' path.

It also has an extension helper inspired by the dllmap system in MonoFramework for cross-platform lib retargetting. This system allows you to specify different names in a 'bindconf' file named after your app to retarget 1 or more libs that rely on Morph-DynaLib for loading.

## Credits

This module was crafted on a foundation of 3 functions:
  - LoadLib
  - UnloadLib
  - DependencyPath

These 3 functions were crafted with reference to BindBC-Loader.

Special Thanks to Mike D Parker.

Referenced source:
https://github.com/BindBC/bindbc-loader/blob/master/source/bindbc/loader/sharedlib.d